<?php
/**
 * Created by PhpStorm.
 * User: pierre-yves
 * Date: 16/09/16
 * Time: 08:38.
 */
namespace pygillier\Nitrogen;

use Silex\Application;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\SessionServiceProvider;
use WhoopsSilex\WhoopsServiceProvider;
use Gaufrette\Filesystem;
use Gaufrette\Adapter\Local as LocalAdapter;
use Silex\Provider\MonologServiceProvider;

/**
 * Configuration.
 *
 * This class handles the configuration of application. All services are defined here and linked together.
 *
 * @author Pierre-Yves Gillier
 */
class Configuration
{
    /**
     * Initialization method.
     *
     * @param Application $app
     */
    public static function init(Application $app)
    {
        //Whoops
        $app->register(new WhoopsServiceProvider());

        // Fragment support
        $app->register(new HttpFragmentServiceProvider());
        $app->register(new SessionServiceProvider());

        // Configuration
        $dotenv = new \Dotenv\Dotenv($app['base_dir']);
        $dotenv->load();

        // Logging
        $app->register(new MonologServiceProvider(), array(
            'monolog.logfile' => $app['base_dir'].'/app/log/development.log',
        ));

        // Twig
        $app->register(new \Silex\Provider\TwigServiceProvider(), array(
            'twig.path' => sprintf('%s/views', $app['base_dir']),
        ));

        // Gaufrette
        $app['gaufrette_service'] = function () use ($app) {
            $adapter = new LocalAdapter(sprintf('%s/images', $app['base_dir']));
            $filesystem = new Filesystem($adapter);

            return $filesystem;
        };

        // Listing Service
        $app['filer'] = function (Application $app) {
            return new Services\FilerService($app['gaufrette_service'], $app['monolog']);
        };

        // Player Service
        $app['player'] = function (Application $app) {
            return new Services\PlayerService($app['gaufrette_service'], $app['monolog'], $app['base_dir']);
        };

        // Global variables
        $app['twig']->addGlobal('name', getenv('NAME'));
        $app['twig']->addGlobal('base_folders', $app['filer']->listBaseFolders());

        // Security
        $app['security.firewalls'] = array(
            'admin' => array(
                'pattern' => '^/',
                'http' => true,
                'users' => array(
                    // raw password is foo
                    'admin' => array('ROLE_ADMIN', getenv('PASS')),
                ),
            ),
        );
        $app->register(new \Silex\Provider\SecurityServiceProvider());
    }
}

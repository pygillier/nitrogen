<?php
/**
 * Created by PhpStorm.
 * User: pierre-yves
 * Date: 15/09/16
 * Time: 22:10.
 */
namespace pygillier\Nitrogen\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class FrontControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];

        // Homepage
        $controllers->get('/', function (Application $app) {
            return $app['twig']->render('index.twig');
        })->bind('homepage');

        // Folder view
        $controllers->get('/folder/{name}', function (Application $app, $name) {
            $files = $app['filer']->getFolderContent($name);

            return $app['twig']->render('folder.twig', array(
                'folder' => $name,
                'files' => $files,
            ));
        })->bind('folder');

        $controllers->get('/play/{folder}/{file}', function (Application $app, $folder, $file) {
            try {
                $app['player']->play($folder, $file);
                $app['session']->getFlashBag()->add('success', "Image ${file} successfully sent to output!");
            } catch (\Exception $e) {
                $app['session']->getFlashBag()->add('error', $e->getMessage());
            }

            return $app->redirect($app['url_generator']->generate('folder', array('name' => $folder)));
        })->bind('play');

        $controllers->get('/replay', function (Application $app, Request $request) {
            $image = $app['player']->getLastPlayedImage();
            $app['player']->play($image->folder, $image->filename);
            if ($request->get('return', false)) {
                $uri = $app['url_generator']->generate('folder', array('name' => $request->get('return')));
            } else {
                $uri = $app['url_generator']->generate('homepage');
            }
            $app['session']->getFlashBag()->add('success', sprintf('Replayed %s !', $image->filename));

            return $app->redirect($uri);
        })->bind('replay');

        $controllers->get('/blackscreen', function (Application $app, Request $request) {
            $app['player']->play('default', 'black.png');
            if ($request->get('return', false)) {
                $uri = $app['url_generator']->generate('folder', array('name' => $request->get('return')));
            } else {
                $uri = $app['url_generator']->generate('homepage');
            }
            $app['session']->getFlashBag()->add('success', 'Black screen displayed !');

            return $app->redirect($uri);
        })->bind('blackscreen');

        $controllers->get('/lastplayed', function (Application $app) {
            return $app['twig']->render('_lastplayed.twig', array(
                'payload' => $app['player']->getLastPlayedImage(),
            ));
        })->bind('lastplayed');

        $controllers->get('/thumbnail/{format}/{folder}/{file}', function (Application $app, $format, $folder, $file) {
            return $app['filer']->getThumbnail($format, $folder, $file);
        })->bind('thumbnail');

        return $controllers;
    }
}

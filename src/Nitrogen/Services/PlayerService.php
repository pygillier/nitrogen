<?php
/**
 * Created by PhpStorm.
 * User: pierre-yves
 * Date: 15/09/16
 * Time: 23:55.
 */
namespace pygillier\Nitrogen\Services;

use Gaufrette\Filesystem;
use Gaufrette\Exception\FileNotFound;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Monolog\Logger;

class PlayerService
{
    const STATUS_FILE = 'lastplayed.json';
    private $filesystem;
    private $base_dir;
    private $logger;

    /**
     * Constructor.
     *
     * @param Filesystem $filesystem Gaufrette interface to file system
     * @param Logger     $logger     Logger interface
     * @param $base_dir Local file system base dir
     */
    public function __construct(Filesystem $filesystem, Logger $logger, $base_dir)
    {
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->base_dir = $base_dir;
    }

    /**
     * Plays an image.
     *
     * The image is displayed through <code>fbi</code> command with options defined in <code>FBI_OPTS</code>.
     *
     * Beforehand, it tries to kill existing <code>fbi</code> executions and continues even if the kill doesn't execute
     * successfully.
     *
     * @param $folder The folder containing the image to play
     * @param $file The image to play
     *
     * @throws \Exception if process encoutered an error
     */
    public function play($folder, $file)
    {
        // Processes
        $pkill_process = new Process('sudo pkill fbi');
        $fbi_process = new Process(vsprintf("sudo /usr/bin/fbi %s '%s'", array(
            getenv('FBI_OPTS'),
            sprintf('%s/images/%s/%s', $this->base_dir, $folder, $file),
        )));
        try {
            $this->logger->addInfo(sprintf('Running process: %s', $fbi_process->getCommandLine()));
            $pkill_process->run();
            $fbi_process->mustRun();

            $infos = array(
                'filename' => $file,
                'folder' => $folder,
                'timestamp' => time(),
            );
            //Write status file
            $this->filesystem->write(self::STATUS_FILE, json_encode($infos), true);
        } catch (ProcessFailedException $e) {
            $this->logger->addError(sprintf('Error during execution of process [%s] : %s',
                $e->getProcess()->getCommandLine(),
                $e->getMessage()
            ));
            throw $e;
        }
    }

    /**
     * Returns the last played image from status file as an array.
     *
     * @return array|mixed An array containing last played image
     */
    public function getLastPlayedImage()
    {
        try {
            $content = $this->filesystem->read(self::STATUS_FILE);

            return json_decode($content);
        } catch (\RuntimeException $e) {
            return array('error' => "Can't read file");
        } catch (FileNotFound $e) {
            return array('error' => 'File not found');
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: pierre-yves
 * Date: 16/09/16
 * Time: 22:09.
 */
namespace pygillier\Nitrogen\Services;

use Gaufrette\Filesystem;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\StreamedResponse;

class FilerService
{
    private $exclusion_list = array(
        'cache',
        'default',
    );

    private $filesystem;

    /**
     * @var \Monolog\Logger
     */
    private $logger;

    public function __construct(Filesystem $filesystem, $logger)
    {
        $this->filesystem = $filesystem;
        $this->logger = $logger;
    }

    public function listBaseFolders()
    {
        return array_filter($this->filesystem->listKeys()['dirs'], function ($item) {
            if (in_array($item, $this->exclusion_list)) {
                return false;
            }

            return strpos($item, '/') === false ? true : false;
        });
    }

    /**
     * Returns list of files in given folder.
     *
     * @param $folder_name The folder
     *
     * @return array List of files
     *
     * @throws \Exception Thrown if given name is not a folder
     */
    public function getFolderContent($folder_name)
    {
        if (!$this->filesystem->isDirectory($folder_name)) {
            throw new \Exception("Item ${folder_name} is not a directory");
        }

        // Get folder content
        $keys = $this->filesystem->listKeys($folder_name);

        $files = array_filter($keys['keys'], function ($key) {
            if (strpos($this->filesystem->mimeType($key), 'image/') === false) {
                return false;
            }

            return true;
        });

        return $files;
    }

    public function getThumbnail($format, $folder, $file)
    {
        switch ($format) {
            case 'list':
                $width = 128;
                $height = 64;
                break;
            case 'lightbox':
                $width = 600;
                $height = 400;
                break;
            case 'sidebar':
                $width = 300;
                $height = null;
            default:
                break;
        }
        $file_uri = sprintf('gaufrette://nitrogen/%s/%s', $folder, $file);
        $cache_uri = sprintf('gaufrette://nitrogen/cache/%s/%s/%s', $format, $folder, $file);
        if (!file_exists($cache_uri)) {
            $this->logger->addInfo("Creating \"${format}\" thumbnail for ${folder}/${file}");
            Image::make(file_get_contents($file_uri))
                ->resize($width, $height, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->save($cache_uri);
        }

        $image = $this->filesystem->get(sprintf('cache/%s/%s/%s', $format, $folder, $file));
        $mime_type = $this->filesystem->mimeType(sprintf('cache/%s/%s/%s', $format, $folder, $file));

        return new StreamedResponse(function () use ($image) {
            echo $image->getContent();
        },
            200,
            array(
                'Content-Type' => $mime_type,
                'Cache-Control' => 'public',
            )
        );
    }
}

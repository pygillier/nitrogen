# Nitrogen

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/9e6af546-4a93-4d84-acee-62a8a3090b8b/mini.png)](https://insight.sensiolabs.com/projects/9e6af546-4a93-4d84-acee-62a8a3090b8b)

This project was built for the [Paris Web conference](https://www.paris-web.fr) . Feel free to use it for your own !

Do you need to display images on a big screen but don't have enough computers to do that ? Nitrogen helps you: using a single Raspberry
Pi on your network, you can display images onscreen without any desktop access, only through web.

## Install

The best way is to use a docker-enabled rPi with docker-compose. Clone the repo, create the
 `.env` file from `.env-dist`, launch `docker-compose up -f docker-compose.pi.yml`, open your browser and go to your raspberry Pi address!
 
Default account is `admin`/`nitrogen`.

## Documentation

* [API doc](docs/index.html)

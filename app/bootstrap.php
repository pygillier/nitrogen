<?php

use Gaufrette\StreamWrapper;

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app['debug'] = true;

$app['base_dir'] = realpath(__DIR__.'/..');

\pygillier\Nitrogen\Configuration::init($app);

// has to be defined here to be valid...
$map = StreamWrapper::getFilesystemMap();
$map->set('nitrogen', $app['gaufrette_service']);
StreamWrapper::register();

$app->mount('/', new \pygillier\Nitrogen\Controllers\FrontControllerProvider());

return $app;

echo "Running CS fixer"
php php-cs-fixer.phar fix . --level=symfony
git add .
git add -u .
echo "Regenerating Documentation"
php sami.phar update app/sami-config.php
echo "Regeneration complete"
echo "Adding updated docs to commit"
git add docs
git add -u docs
echo